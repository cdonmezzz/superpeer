import 'bootstrap';
import 'jquery';
import 'jquery-mousewheel';
import './scss/styles.scss';

$(document).ready(function () {
  //Video Anim
  const videoAnimStyle = () => {
    $('.video-content style').html(`
      .video-content.active .movie{
        top: ${$('.video-content').offset().top + 30}px;
        width: ${parseInt($('.video-content').width()) - 120}px;
        height: ${parseInt($('.video-content').height()) - 62}px;
      }
    `);
  };

  //+/SCROLL/+//
  const websiteScrollEvent = () => {
    //add active
    var scrollTop = $(window).scrollTop();
    $('.anim').each(function (i) {
      var $box = $('.anim').eq(i);
      var offset = $box.offset();
      if (scrollTop > offset.top - $(window).height() / 2) {
        $box.addClass('active');
      } else {
        $box.removeClass('active');
      }
    });

    //r locked
    if (scrollTop > 1) {
      $('.video-content').addClass('active');
      $('body').removeClass('locked');
    } else {
      $('.video-content').removeClass('active');
      $('body').addClass('locked');
    }

    //mouse scroll flow
    document.body.style.setProperty(
      '--scroll',
      window.pageYOffset / (document.body.offsetHeight - window.innerHeight)
    );
  };

  //Add if class exists
  const websiteMouseWheelEvent = (event) => {
    if ($('body').hasClass('locked') && event.originalEvent.deltaY > 0) {
      $('.video-content').addClass('active');
      setTimeout(() => {
        $('body').removeClass('locked');
      }, 1000);
    }
  };

  videoAnimStyle();
  $(window).resize(videoAnimStyle);

  websiteScrollEvent();
  $(window).scroll(websiteScrollEvent);

  $(window).on('mousewheel', websiteMouseWheelEvent);

  //Navbar Toggle
  $('.toggle').on('click', function () {
    $('.toggle').toggleClass('on-off');
    $('.navbar').toggleClass('navbar-active');
  });
});
